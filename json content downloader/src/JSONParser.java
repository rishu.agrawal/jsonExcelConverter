import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Vector;

import com.google.gson.Gson;

public class JSONParser {
	public Page[] pageList;
	public String readUrl(String urlString) throws Exception {
	    BufferedReader reader = null;
	    try {
	        URL url = new URL(urlString);
	        reader = new BufferedReader(new InputStreamReader(url.openStream()));
	        StringBuffer buffer = new StringBuffer();
	        int read;
	        char[] chars = new char[1024];
	        while ((read = reader.read(chars)) != -1)
	            buffer.append(chars, 0, read); 
	        return buffer.toString();
	    } finally {
	        if (reader != null)
	            reader.close();
	    }
	}
	
	public class Cuts {
		String x1,x2,x3,x4;
	}
	
	public class Header_Image {
		String base;
		Cuts cuts;
	}
	
	public class Sq_Image {
		String base;
		Cuts cuts;
	}
	
	public class Image {
	    Header_Image header_image;
	    Header_Image category_player_image;
	    String type;
	}
	
	public class Content {
		String title;
		String thumbnail_url;
		String type;
		String section;
		Sq_Image sq_image;
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getThumbnail_url() {
			return thumbnail_url;
		}
		public void setThumbnail_url(String thumbnail_url) {
			this.thumbnail_url = thumbnail_url;
		}
		public Sq_Image getSq_image() {
			return sq_image;
		}
		public void setSq_image(Sq_Image sq_image) {
			this.sq_image = sq_image;
		}
	}
	
	public class Page {
	    String title;
	    String type;
	    Image image;
	    ArrayList<Content> content;
	}
	
	public void parseData() throws Exception {
		String json = readUrl("http://localhost:8080/js/data.json");
		
	    Gson gson = new Gson();    
	    pageList = gson.fromJson(json, Page[].class);
	}

	public String getHeaderImageURL(Page p) {
		String url;
		url = new String(p.image.header_image.base+p.image.header_image.cuts.x1+"\n"+p.image.header_image.base+p.image.header_image.cuts.x2+"\n"+p.image.header_image.base+p.image.header_image.cuts.x3+"\n"+p.image.header_image.base+p.image.header_image.cuts.x4);
		return url;
	}
	
	public String getPlayerImageURL(Page p) {
		String url;
		url = new String(p.image.category_player_image.base+p.image.category_player_image.cuts.x1+"\n"+p.image.category_player_image.base+p.image.category_player_image.cuts.x2+"\n"+p.image.category_player_image.base+p.image.category_player_image.cuts.x3+"\n"+p.image.category_player_image.base+p.image.category_player_image.cuts.x4);
		return url;
	}
	
	public String getSqImageURL(Content content) {
		String url;
		url = new String(content.sq_image.base+content.sq_image.cuts.x1+"\n"+content.sq_image.base+content.sq_image.cuts.x2+"\n"+content.sq_image.base+content.sq_image.cuts.x3+"\n"+content.sq_image.base+content.sq_image.cuts.x4);
		return url;	
	}
}
