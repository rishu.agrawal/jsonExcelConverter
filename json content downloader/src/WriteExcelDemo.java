import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class WriteExcelDemo 
{
	static JSONParser parser;
	
	public static void main(String[] args) throws Exception {
		int index = 0;
		parser = new JSONParser();
		parser.parseData();
	    XSSFWorkbook workbook = new XSSFWorkbook();
	    XSSFSheet sheet = workbook.createSheet("Assets");
	    Map<String, ArrayList<String>> data = new TreeMap<String, ArrayList<String>>();
	    
	    for (int i=0; i < parser.pageList.length; i++) {
	    	index++;
	    	ArrayList<String> category = new ArrayList<String>();
	    	category.add(parser.pageList[i].title);
	    	category.add(parser.getHeaderImageURL(parser.pageList[i]));
	    	category.add(parser.getPlayerImageURL(parser.pageList[i]));
	    	data.put(String.valueOf(index), category );
	    	for (int j=0; j < parser.pageList[i].content.size(); j++) {
	    		index ++;
	    		category = getValuesFromContent(parser.pageList[i].content.get(j));
	    		data.put(String.valueOf(index), category);
			}
	    }
	    
	    Set<String> keyset = data.keySet();

	    int rownum = 0;
	    for (String key : keyset) 
	    {
	        Row row = sheet.createRow(rownum++);
	        ArrayList<String> objArr = data.get(key);
	        int cellnum = 0;
	        for (Object obj : objArr) 
	        {
	            Cell cell = row.createCell(cellnum++);
	            if (obj instanceof String) 
	            {
	                cell.setCellValue((String) obj);
	            }
	            else if (obj instanceof Integer) 
	            {
	                cell.setCellValue((Integer) obj);
	            }
	        }
	    }
	    try 
	    {
	        //Write the workbook in file system
	        FileOutputStream out = new FileOutputStream(new File("/Users/agrawalr/Documents/Output.xlsx"));
	        workbook.write(out);
	        out.close();
	    } 
	    catch (Exception e)
	    {
	        e.printStackTrace();
	    }
	}
	
	public static ArrayList<String> getValuesFromContent(JSONParser.Content content ) {
		ArrayList <String> values = new ArrayList<String>();
		values.add(content.section);
		values.add(content.type);
		values.add(content.title);
		values.add(content.thumbnail_url);
		if (content.type.equals("Question")) {
			values.add(parser.getSqImageURL(content));
		}
		return values;
	}
}